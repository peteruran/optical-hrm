#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy import constants

sampling_freq = 500
upper_pulse = 200/60
lower_pulse = 40/60

N = 20 # filter order
cutoff_freq = 200 # Hz
Wn = cutoff_freq / (sampling_freq/2) # normalize to half of sampling freq. Nyquist is at W_n = 1.0
b, a = signal.butter(N, Wn, btype='low')
w, h = signal.freqz(b, a)

plt.plot(w, abs(h))
plt.title('Butterworth filter frequency response')
plt.xlabel('Frequency')
plt.show()
