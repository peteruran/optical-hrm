#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import os
import time
import sys
from scipy import signal
from scipy import constants

## Set working directory to script location
os.chdir(os.path.dirname(sys.argv[0]))

plt.rcParams.update({'font.size': 11})

## Channel constants
RED = 0
GREEN = 1
BLUE = 2

labels = ('Red', 'Green', 'Blue')
colors = ('C1', 'C2', 'C0')

## Data
bpm = 		[97,98,100,107,60,60,60,106,107,128,139,98,99,105]
bpm_est = 	[75,91.43,111,123.00,65.78,77.42,83.72,109.09,110.34,112.94,115.66,100.00,106.67,105.50]
bpm_2_est = [39.3,104.30,114.00,120.00,40.68,80.00,92.31,109.09,109.09,114.29,120.00,100.00,109,126.32]
col = 		[1,1,1,1,0,0,0,0,0,0,0,1,0,0,1]

lin_bpm = np.polyfit(bpm, bpm_est, 1)
lin_bpm2 = np.polyfit(bpm, bpm_2_est, 1)


plt.title('Measurements')

for x,y1,y2,c in zip(bpm, bpm_est, bpm_2_est, col):
	plt.plot(x, y1, 'o', color=colors[c])
	#plt.plot(x, y2, 'v', color=colors[c])

plt.plot(bpm, np.poly1d(lin_bpm)(bpm), '-', color='C3')
#plt.plot(bpm, np.poly1d(lin_bpm2)(bpm), '-', color='C4')

plt.xlabel('Actual BPM')
plt.ylabel('Estimated BPM')
plt.grid()
plt.show()
