#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import os
import time
import sys
from scipy import signal
from scipy import constants

## Set working directory to script location
os.chdir(os.path.dirname(sys.argv[0]))

# Channel constants
RED = 0
GREEN = 1
BLUE = 2

## Initial settings
#filename = sys.argv[1]
filename = './data/tl_100'
sampling_freq = 40

print('Import data...')
raw_data = np.loadtxt(filename)
data = signal.detrend(raw_data[550:1000], axis=0)

# add white noise
mean = 0
std = 1
#data += np.random.normal(0, std, size=data.shape)


filtered_data = signal.savgol_filter(data, 13, 3, axis=0)
data = filtered_data

print('Data has', str(raw_data[:,0].size), 'samples')
samples, channels = data.shape
print('Numer of samples per channel:', data.size/channels)



## Find mean lag between elements in array
def mean_lag(a):
	return np.array([a[n+1]-a[n] for n in range(len(a)-1)]).mean()


## Calculate BPM by autocorrelation
def find_bpm(corr, sampling_freq):
	peaks = signal.find_peaks(corr, prominence=0.01)
	peaks = peaks[0] # remove metadata
	peaks = np.fft.ifftshift(peaks)[0:int(peaks.size/2)][0:5] # take only 5 positive points, because of symmetry
	bpm = 60 / (mean_lag(peaks) * (1/sampling_freq))
	bpm_2 = 60 / (mean_lag(peaks[0:2]) * (1/sampling_freq))
	print('Autocorr. BPM:', np.round(bpm,2))
	print('Two peak estimate', np.round(bpm_2,2))
	return (bpm, bpm_2, peaks)

'''
## SNR by autocorrelation
signal_power = np.array([psd[n] for n in peaks]).sum()
noise_power = psd.sum() - signal_power
snr = 10*np.log10(signal_power/noise_power)
print('SNR autocorr: ', snr, 'dB')
'''


def find_snr(psd, freqs):
	psd = psd / psd.sum() # normalize
	psd = np.fft.ifftshift(psd)[0:int(psd.size/2)] # only use positive frequencies
	freqs = np.fft.ifftshift(freqs)[0:int(freqs.size/2)] # same
	peaks = signal.find_peaks(psd, height=0.001, distance=15)
	peaks = [x for x in peaks[0] if x > 0.5*sampling_freq/2]
	widths = np.array(signal.peak_widths(psd, peaks)[2:4]).flatten() # get interpolation points
	widths = np.rint(widths).astype(np.int) # round to integers for indexes
	print('WIDTHS:', widths)

	#print('peaks', peaks)
	#print('widths', widths)

	# calculate snr and make plot of detected signal
	# manual hack
	#widths = [20,26]
	sig = psd[widths[0]:widths[1]]
	signal_power = sig.sum()
	noise_power = 1 - signal_power
	snr = 10*np.log10(signal_power/noise_power)

	print('SNR:', np.round(snr,2), 'dB')

	power_plot = np.zeros(psd.size)
	power_plot[widths[0]:widths[1]] = sig


	plt.plot(freqs,psd, '-D', markevery=peaks)
	plt.plot(freqs,power_plot)
	plt.show()


## Calculating FFT
spectrum = np.abs(np.fft.fftshift(np.fft.fft(data, axis=0), axes=0)) # very important to specify axes in fftshift

## Processing
channel = GREEN

fft_freqs = np.fft.fftshift(np.fft.fftfreq(samples, 1/sampling_freq))

corr = np.apply_along_axis(lambda m: signal.correlate(m, m, mode='same'), axis=0, arr=data)
corr *= 1/corr.max(axis=0)
# Calculate autocorrelation and find BPM
 #normalize
(bpm, bpm_2, peaks) = find_bpm(corr[:,channel], sampling_freq)

psd = np.abs(np.fft.fftshift(np.fft.fft(corr, axis=0)))
#find_snr(psd[:,channel], fft_freqs)




### PLOTTING

## Axis' for plotting

bpm_scale = fft_freqs * 60
corr_axis = np.linspace(-samples/2, samples/2, corr[:,0].size)

labels = ('Red', 'Green', 'Blue')
colors = ('C1', 'C2', 'C0')

plt.rcParams.update({'font.size': 11})


## Data plot
plt.subplot(2,2,1)
plt.title('Data')
for dat,lab,col in zip(data.T, labels, colors):
	data_plots = plt.plot(dat,label=lab,color=col)
plt.legend(loc='upper right')
plt.xlabel('Sample [n]')
plt.grid()


## Spectrum plot
plt.subplot(2,2,2)
plt.title('FFT Spectrum')
for spec,lab,col in zip(spectrum.T, labels, colors):
	plt.plot(fft_freqs,spec,label=lab,color=col)
#plt.plot(fft_freqs, spectrum[:,0])
plt.xlim((-4,4))
plt.legend(loc='upper right')
plt.xlabel('Frequency [Hz]')
plt.grid()



## Autocorrelation plot
plt.subplot(2,2,3)
plt.title('Autocorrelation')
#for c,lab,col in zip(corr.T, labels, colors):
#	plt.plot(corr_axis, c, '-D', label=lab,color=col, markevery=peaks.tolist())
plt.plot(corr_axis, corr[:,channel], '-D', label=labels[channel],color=colors[channel], markevery=peaks.tolist())
plt.legend(loc='upper right')
plt.xlabel('Lag [l]')
plt.grid()

## Power spectrum plot
plt.subplot(2,2,4)
psd_plt = plt.plot(fft_freqs, psd[:,channel] / psd[:,channel].sum(), label=labels[channel],color=colors[channel])
plt.legend(loc='upper right')
plt.title('Power spectral density')
plt.xlim((-4,4))
plt.xlabel('Frequency [Hz]')
plt.grid()

plt.show()
