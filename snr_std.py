#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import os
import time
import sys
from scipy import signal
from scipy import constants

## Set working directory to script location
os.chdir(os.path.dirname(sys.argv[0]))

plt.rcParams.update({'font.size': 11})

## Channel constants
RED = 0
GREEN = 1
BLUE = 2

labels = ('Red', 'Green', 'Blue')
colors = ('C1', 'C2', 'C0')

## Data
rel = 	[0.77,0.93,1.10,1.29,1.40,1.03,1.03,0.88,0.83,1.02,1.08,1.00]
rel1 =	[1 - n for n in rel]
snr = 	[-18.71,-13.40,-13.77,-6.65,-16.08,-16.40,-7.76,-6.55,-19.60,-8.00,-19.50,-18.20]

lin = np.polyfit(rel1, snr, 1)
lin_fn = np.poly1d(lin)



plt.title('Relative error vs. SNR')

plt.plot(rel1, snr, 'o', color='C4')
plt.plot(rel1, lin_fn(rel1), '-', color='C3')
plt.xlabel('Relative error')
plt.ylabel('Estimated SNR')
plt.grid()
plt.show()
